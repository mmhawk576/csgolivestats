var express = require('express');
var router = express.Router();
var SocketHandler = require("../controllers/SocketController");
var DBController = require("../controllers/DBController");
var weapon_urls = require("../items_game_cdn.json");
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post("/", function(req, res, next){
  var gamestate = JSON.parse(JSON.stringify(req.body));
  res.writeHead(200, {"Content-Type": "text/html"});
  //res.end('');
  if(gamestate.player && gamestate.player.weapons){
    for(var index in gamestate.player.weapons){
        var weapon = gamestate.player.weapons[index];
        if(weapon.paintkit){
          weapon.paintedName = weapon.paintkit == "default" ? weapon.name : weapon.name + "_" + weapon.paintkit;
          weapon.url = weapon_urls[weapon.paintedName];
        }
    }
  }
  if(gamestate.hasOwnProperty('map') && gamestate.map.phase == "gameover"){
      var matchstats = gamestate.player.match_stats;

      DBController.addStatsEntry(gamestate.player.steamid, gamestate.map.mode, matchstats.kills, matchstats.assists, matchstats.deaths, matchstats.mvps, function(err){
        if(err){
            console.log(err);
            return;
        }
          console.log("Log added successfully");
      });
  }
  SocketHandler.sendToClient(gamestate);
});

module.exports = router;
