/**
 * Created by sad on 12/12/2015.
 */

var http = require("http");
var https = require("https");
var DBController = require('./DBController');

var unassociatedSockets = [];
var associatedConnections = [];

function AssociatedConnection(socket, id){
    return{
        socket:socket,
        steamid:id,
        playerInfo:null
    };
}

function startServerSocket(server){
    var io = require('socket.io').listen(server);
    io.sockets.on('connection', function(socket){
        setupClientSocket(socket);
        unassociatedSockets.push(socket);
    });
}

function connectionComplete(socket, steamid){
    disconnectSocket(socket);
    var connection = AssociatedConnection(socket, steamid);

    var route = "/ISteamUser/GetPlayerSummaries/v0002/";
    var parameters = "steamids=" + steamid;
    makeSteamApiRequest(route, parameters, function(data){
        connection.playerInfo = data.response.players[0];
        associatedConnections.push(connection);
        socket.emit("connected", connection.playerInfo);
        console.log(associatedConnections);
    });

}

function disconnectSocket(socket){
    for(var key in unassociatedSockets){
        var unassociatedSocket = unassociatedSockets[key];
        if(socket == unassociatedSocket){
            unassociatedSockets.splice(key, 1);
            return;
        }
    }
    for(var key in associatedConnections){
        var connection = associatedConnections[key];
        if(socket == connection.socket){
            associatedConnections.splice(key, 1);
        }
    }
}

function setupClientSocket(socket){
    socket.on("setup",function(data){
        //for(var key in associatedConnections){
        //    if(associatedConnections[key].steamid == data){
        //        socket.emit("message", "Your account is already connected");
        //        return;
        //    }
        //}

        connectionComplete(socket, data);

    });

    socket.on("translateToID", function(data){
        var route = "/ISteamUser/ResolveVanityURL/v0001/";
        var parameters = "vanityurl=" + data;
        makeSteamApiRequest(route, parameters, function(data){
            console.log(data);
            var response = data["response"];
            var status = response["success"];
            if(status == 1){
                connectionComplete(socket, response.steamid);
            }
        })
    });

    socket.on('getMatchInformation', function(){
        var steamid;
        for(var comparisonIndex in associatedConnections){
            var comparisionConnection = associatedConnections[comparisonIndex];
            if(comparisionConnection.socket == socket){
                steamid = comparisionConnection.steamid;
                break;
            }
        }
        if(steamid) {
            console.log("found steam id");
            DBController.getStatsForSteamID(steamid, function (matchdata) {
                console.log("emiting match data");
                socket.emit("matchInformation", matchdata);
            });
            return;
        }
        console.log("Failed to find steam ID");
    });

    socket.on('disconnect', function(){
       console.log("Socket Closed");
    });

    socket.on('logout', function(){
        disconnectSocket(socket);
        socket.emit("logoutComplete");
    });
}

function makeSteamApiRequest(route, parameters, callback){
    var host = "api.steampowered.com";
    var options = {
        host: host,
        port: 80,
        path: route + "?key=DFD525209F1309AFD10E461EABB5047C&" + parameters,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    var prot = options.port == 443 ? https : http;
    var steamReq = prot.request(options, function(steamRes)
    {
        var output = '';
        console.log(options.host + ':' + steamRes.statusCode);
        steamRes.setEncoding('utf8');

        steamRes.on('data', function (chunk) {
            output += chunk;
        });

        steamRes.on('end', function() {
            var json = JSON.parse(output);
            callback(json);

        });
    });

    steamReq.on('error', function(err) {
        console.log("Error getting steam stuff" + err);
        socket.emit("message", "connection failed")
    });

    steamReq.end();
}

function sendToClient(data){
    //console.log(data);
    for(var index in associatedConnections){
        var connection = associatedConnections[index];
        if (data["player"]["steamid"] == connection.steamid) {
            connection.socket.emit("playerInfo", data);
            console.log("Sending to Steam Id:" + connection.steamid + ", " +connection.playerInfo.personaname);
        }
    }
}

module.exports = {
    startServer: startServerSocket,
    sendToClient: sendToClient
};