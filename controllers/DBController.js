/**
 * Created by sad on 12/17/2015.
 */

var pg = require("pg");

var user = "postgres";
var pass = "csgostatspwd";
var connectionString = process.env.DATABASE_URL || 'postgres://' + user + ":" + pass + '@localhost:5432/csgostatsdb';


var playersWithRecentlyCompleteGames = [];

var gameModesEnum = {
    competitive: "competitive"
};

//setup table
pg.connect(connectionString, function(err, client, done) {
    if(err){
        console.log(err);
        return;
    }
    var query = client.query("CREATE TABLE matchstats(steamid varchar (50), gametype varchar (50), kills smallint, assists smallint, deaths smallint, mvps smallint)");
    query.on('error', function(error){
        console.log(error);
        done();
    });

    query.on('end', function(){
        done();
    });
});



var tablename = "matchstats";

function getStatsForSteamID(steamID, callback){

    pg.connect(connectionString, function(err, client, done){
        if(err){
            done();
            callback(null, err);
            return null;
        }

        var results = [];
        var query = client.query("SELECT * FROM " + tablename + " WHERE steamid=($1)", [steamID]);

        query.on('row', function(row){
            results.push(row);
        });

        query.on('end', function(){
            done();
            callback(results, null);
        })
    });
}

function addStatsEntry(steamID, gameType, kills, assists, deaths, mvps, callback){
    var minsToBlockFor = 2;
    //This if block prevents games being added to the database multiple times.
    //This just prevents adding to the database for a steamID for 30 seconds.
    if(playersWithRecentlyCompleteGames.indexOf(steamID) != -1){
        return;
    }
    else{
        playersWithRecentlyCompleteGames.push(steamID);
        setTimeout(function(){
            var playerIndex = playersWithRecentlyCompleteGames.indexOf(steamID);
            playersWithRecentlyCompleteGames.splice(playerIndex, 1);
        }, minsToBlockFor * 60 * 1000);
    }

    pg.connect(connectionString, function(err, client, done){
        if(err){
            done();
            callback(err);
            return null;
        }

        var query = client.query("INSERT INTO " + tablename + "(steamid, gametype, kills, assists, deaths, mvps)" +
            "values($1, $2, $3, $4, $5, $6)", [steamID, gameType, kills, assists, deaths, mvps]);

        query.on('error', function(error){
            done();
            callback(error);
        });

        query.on('end', function(){
            done();
            callback(null);
        });
    });
}

module.exports = {
    getStatsForSteamID: getStatsForSteamID,
    addStatsEntry: addStatsEntry
};